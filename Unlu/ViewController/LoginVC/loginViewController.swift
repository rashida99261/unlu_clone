import UIKit

class loginViewController: UIViewController {
    
    @IBOutlet weak var veiwLogin : UIView!

    @IBOutlet weak var txtUserName: RCustomTextField!
  
    @IBOutlet weak var txtPassword: RCustomTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    func setUpUI(){
        self.veiwLogin.layer.cornerRadius = 10
    }
    func validation(){
        if txtUserName.text == "" && txtPassword.text == ""{
            showAlertVC(title: alertTitle, message: "Please enter email id and password", controller: self)
        }else if txtUserName.text == ""{
            showAlertVC(title: alertTitle, message: "Please enter email id", controller: self)

        }else if txtPassword.text == ""{
            showAlertVC(title: alertTitle, message: "Please enter password", controller: self)

        }else{
            callWebServiceForLogin()
        }
    }

    @IBAction func clikcONLoginBtn(_ sender: UIButton){
       // let home = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
        validation()
    }
}

extension loginViewController {
    func callWebServiceForLogin() {
        WebServiceManager.showLoaderView(view: self.view)
        let url : String = AppShareData.serverURL + AppShareData.EndPoints.loginApi
        let param = ["grant_type":"password",
                     "username": txtUserName.text!,
                     "password": txtPassword.text!,
        "client_id":"unlo-android",
        "client_secret":"OurClearPassword"]
    let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        objWebServiceManager.requestPost(strURL: url, params: param, headers: headers, success: { (response) in
            print(response)
            WebServiceManager.hideLoaderView(view: self.view)
            self.showToast(message: "Login SuccessFully")
                   if !response.isEmpty{
              
                    userDefault.set(response["access_token"], forKey: AppShareData.UserDefaultKeys.access_token)
                    userDefault.set(response["expires_in"], forKey: AppShareData.UserDefaultKeys.expires_in)
                    userDefault.set(response["refresh_token"], forKey: AppShareData.UserDefaultKeys.refresh_token)
                    userDefault.set(response["scope"], forKey: AppShareData.UserDefaultKeys.scope)
                    userDefault.set(response["token_type"], forKey: AppShareData.UserDefaultKeys.token_type)
                    userDefault.set(self.txtUserName.text!, forKey: AppShareData.UserDefaultKeys.userName)
                    userDefault.set(true, forKey: AppShareData.UserDefaultKeys.isFirstTime)
                    userDefault.synchronize()
                    AppShareData.Variable.isFromLogin = true
                let home = self.storyboard?.instantiateViewController(withIdentifier: "homeViewController") as! homeViewController
                self.navigationController?.pushViewController(home, animated: true)
            }
           
        }) { (error) in
            print(error)
            WebServiceManager.hideLoaderView(view: self.view)
            self.showToast(message: "Something Went Wrong")
        }

    }
}
