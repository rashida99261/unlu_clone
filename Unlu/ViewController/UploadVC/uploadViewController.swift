//
//  uploadViewController.swift
//  Unlu
//
//  Created by Apple on 23/04/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class uploadViewController: UIViewController {
    
    @IBOutlet weak var collVideos : UICollectionView!
    
    var isPopFrom = ""

    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        
        if(isPopFrom == "user"){
            self.navigationController?.popViewController(animated: true)
        }
        else if(isPopFrom == "request"){
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: userRequestViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    @IBAction func clickONUploadBtn(_ sender : UIButton)
    {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "recordViewController") as! recordViewController
        self.navigationController?.pushViewController(dest, animated: true)
    }
}

extension uploadViewController : UICollectionViewDelegate , UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collVideos.dequeueReusableCell(withReuseIdentifier: "videoCell", for: indexPath) as! videoCell
        
        if(indexPath.item  == 0)
        {
            cell.viewOpenGallery.isHidden = false
            cell.imgVideo.isHidden = true
            cell.viewTime.isHidden = true
        }
        else{
            
            cell.viewOpenGallery.isHidden = true
            cell.imgVideo.isHidden = false
            cell.viewTime.isHidden = false
            
            cell.imgVideo.image = UIImage(named: "coll_img")
            
            
        }
        return cell
    }
    
}

class videoCell : UICollectionViewCell
{
    @IBOutlet weak var imgVideo : UIImageView!
    @IBOutlet weak var viewOpenGallery : UIView!
    @IBOutlet weak var viewTime : UIView!
}
