//
//  recordViewController.swift
//  Unlu
//
//  Created by Apple on 25/04/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
//import CameraManager
import Photos

class recordViewController: FilterCamViewController {
    
    @IBOutlet weak var cameraView: UIView!
    
  //  let cameraManager = CameraManager()
    var imagePicker: ImagePicker!
    
    @IBOutlet weak var viewProgressbar: UIView!
    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var lblRecord: UILabel!
    
     @IBOutlet weak var viewMsg: UIView!
    
    @IBOutlet weak var viewSubmit: UIView!
    @IBOutlet weak var viewTabs: UIView!
    @IBOutlet weak var viewCC: UIView!
    @IBOutlet weak var viewBeauty: UIView!
    @IBOutlet weak var btnPlay: UIButton!
    
    @IBOutlet weak var viewBottomUpload: UIView!
    @IBOutlet weak var viewBottomErase: UIView!
    
    var isSelectRecordBtn = "false"
    
    private let myFilters: [[CIFilter]] = [
        [],
        [CIFilter(name: "CIPhotoEffectInstant")!],
        [CIFilter(name: "CIPhotoEffectInstant")!, CIFilter(name: "CIPhotoEffectNoir")!],
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cameraDelegate = self
        shouldShowDebugLabels = true

        self.setUpCamera()
        self.setUpUI()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        cameraManager.askUserForCameraPermission({ permissionGranted in
//            if permissionGranted {
//                       self.addCameraToView()
//                   }
//            })
               
         //   cameraManager.cameraOutputMode =  CameraOutputMode.videoWithMic
//            switch (cameraManager.cameraOutputMode) {
//
//            case .stillImage:
//                self.cameraView.isHidden = false
//
//            case .videoWithMic, .videoOnly:
//
//                self.cameraView.isHidden = false
//
//
//                   //sender.setTitle("Video", for: UIControlState())
//            }
               
         //   cameraManager.resumeCaptureSession()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
     //   cameraManager.stopCaptureSession()
    }
    
    
    private func saveVideoToPhotos(_ url: URL) {
        let save = {
            PHPhotoLibrary.shared().performChanges({ PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url) }, completionHandler: { _, _ in
                let fileManager = FileManager.default
                if fileManager.fileExists(atPath: url.path) {
                    try? fileManager.removeItem(at: url)
                }
            })
        }
        if PHPhotoLibrary.authorizationStatus() != .authorized {
            PHPhotoLibrary.requestAuthorization { status in
                if status == .authorized {
                    save()
                }
            }
        } else {
            save()
        }
    }
}

//Btn Actions
extension recordViewController{
    
    @IBAction func clickONBackBtn(_ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clikcONBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            
            //flip btn
        }
        else if(sender.tag == 20){
            // record btn
            if isSelectRecordBtn == "false" {
                    isSelectRecordBtn = "true"
                    self.viewProgressbar.isHidden = false
                
         //       cameraManager.startRecordingVideo()
                startRecording()
                    self.lblRecord.text = "Pause Recording"
                    btnRecord.setImage(UIImage(named: "recode_start"), for: .normal)
                
                    self.viewBottomErase.isHidden = false
                    self.viewBottomUpload.isHidden = true
                           
            }else if isSelectRecordBtn == "true"{
                    isSelectRecordBtn = "play"
                    self.lblRecord.text = "Continue"
                    btnRecord.setImage(UIImage(named: "play"), for: .normal)
                stopRecording()
          //   cameraManager.stopVideoRecording({ (_, error) -> Void in
//                    guard let videoURL = videoURL else {
//                        //Handle error of no recorded video URL
//                        return
//                    }
//                    do {
//                        try FileManager.default.copyItem(at: videoURL, to: videoURL)
//                    }
//                    catch {
//                        //Handle error occured during copy
//                    }
        //        })
                 
            }
            else if isSelectRecordBtn == "play"{
                    isSelectRecordBtn = "true"
                    self.lblRecord.text = "Pause Recording"
                    btnRecord.setImage(UIImage(named: "recode_start"), for: .normal)
                
                    self.viewBottomErase.isHidden = false
                    self.viewBottomUpload.isHidden = true
            }
            
        }
        else if(sender.tag == 30){
            
            //upload btn will open gallery
            self.imagePicker.present(from: sender)
        }
        else if(sender.tag == 40){
            
            //cc btn
            UIView.transition(with: view, duration: 0.5, options: .curveEaseInOut, animations: {
                self.viewMsg.isHidden = false
            })
            
        }
        else if(sender.tag == 50){
            //beauty btn
        }
        else if(sender.tag == 60){
            //msg view close btn
            UIView.transition(with: view, duration: 0.5, options: .curveEaseInOut, animations: {
                    self.viewMsg.isHidden = true
            })
        }
        else if(sender.tag == 70){
            //erase btn
        }
        else if(sender.tag == 80){
            //done btn
            UIView.transition(with: view, duration: 0.5, options: .curveEaseInOut, animations: {
                    self.viewSubmit.isHidden = false
                    self.viewTabs.isHidden = true
                    self.viewCC.isHidden = true
                    self.viewBeauty.isHidden = true
                    self.btnPlay.isHidden = false
            })
         
        }
        
        
    }
    
    
    @IBAction func clickONSubmitBtn(_ sender: UIButton)
    {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "requestViewController") as! requestViewController
        isUpload = "true"
        self.navigationController?.pushViewController(dest, animated: true)
    }
}

extension recordViewController {
    
    func setUpUI()
    {
         self.viewProgressbar.isHidden = true
        //self.viewMsg.isHidden = true
         self.viewBottomErase.isHidden = true
         self.viewBottomUpload.isHidden = false
        
        self.viewSubmit.isHidden = true
        self.viewTabs.isHidden = false
        self.btnPlay.isHidden = true
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        
    }
    
    func setUpCamera()
    {
    self.cameraView.isHidden = false
        
       //  cameraManager.showAccessPermissionPopupAutomatically = true
                
                
      //   let currentCameraState = cameraManager.currentCameraStatus()
      //   if currentCameraState == .notDetermined {
                   // askForPermissionsButton.isHidden = false
                    
      //  } else if (currentCameraState == .ready) {
      //      addCameraToView()
       // }
                
    }
    
    // MARK: - Open Camera
   // fileprivate func addCameraToView()
//    {
//    //    cameraManager.addPreviewLayerToView(cameraView, newCameraOutputMode: CameraOutputMode.stillImage)
//    //    cameraManager.showErrorBlock = { [weak self] (erTitle: String, erMessage: String) -> Void in
//
//            let alertController = UIAlertController(title: erTitle, message: erMessage, preferredStyle: .alert)
//            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alertAction) -> Void in  }))
//
//            self?.present(alertController, animated: true, completion: nil)
//        }
  //  }
}

extension recordViewController: FilterCamViewControllerDelegate {
    func filterCamDidStartRecording(_: FilterCamViewController) {}

    func filterCamDidFinishRecording(_: FilterCamViewController) {}

    func filterCam(_: FilterCamViewController, didFinishWriting outputURL: URL) {
        saveVideoToPhotos(outputURL)
    }

    func filterCam(_: FilterCamViewController, didFocusAtPoint _: CGPoint) {}

    func filterCam(_: FilterCamViewController, didFailToRecord _: Error) {}
}
//upload extension
extension recordViewController: ImagePickerDelegate {

    func didSelect(image: UIImage?) {
        //picked image will return here
    }
}
