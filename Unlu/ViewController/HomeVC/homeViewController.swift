//
//  homeViewController.swift
//  Unlu
//
//  Created by Apple on 11/04/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
//import TinderSwipeView


class homeViewController: UIViewController {
    
      var originalPoint = CGPoint.zero
    
        private var swipeView: TinderSwipeView<UserRequestModel>!{
           didSet{
               self.swipeView.delegate = self
           }
        }
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewLeftGuide: UIView!
    @IBOutlet weak var viewRightGuide: UIView!
    @IBOutlet weak var viewRewindGuide: UIView!
    @IBOutlet weak var viewNoData: UIView!
    
    var currentCard : TinderCard!
    var arrUserRequest = [UserRequestModel]()
    
    var userModels = [userModel]() // =  {
//        var model : [userModel] = []
//        for n in 1...2 {
//            model.append(userModel(Username: "Akshay Pruthi", Usermsg: "Akshay Pruthi wants to wish his Friend a \n Happy Birthday", num: "\(n)"))
//
//        }
//        return model
//    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(userDefault.value(forKey: AppShareData.UserDefaultKeys.access_token)!)
        print(userDefault.value(forKey: AppShareData.UserDefaultKeys.token_type)!)
        if  AppShareData.Variable.isFromLogin{
            callWebserviceForRequest(header: [:])
        }else{
        let strAuthToken=UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.access_token)!
            let strTypeToken = UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.token_type)!
            
                let headers = ["Authorization" : strTypeToken + " " + strAuthToken]

            callWebserviceForRequest(header: headers)

        }
   //     self.setUpUI(isFrstTime: true, model: userModels)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.swipeToPop()
    }
}
//MARK:- CallWebService
extension homeViewController {

    func callWebserviceForRequest(header:[String:Any])  {
        WebServiceManager.showLoaderView(view: self.view)
        let url : String = AppShareData.serverURL + AppShareData.EndPoints.FreshVideoReques
        objWebServiceManager.requestGet(strURL: url, params: nil, headers: header, success: { (response) in
            print(response)
            WebServiceManager.hideLoaderView(view: self.view)

            let strStatus = response["statusCode"] as? Int
            if strStatus == 0 {
                let dict = response["payload"] as? [AnyObject]
                if dict!.isEmpty{
                    print("blank")
                    self.viewNoData.isHidden = false
                }else{
                    print(dict!)
                    self.parseRequest(arrRequest: dict!)
                    self.viewNoData.isHidden = true
                }
            }
            
            else if strStatus == 400{
                
                self.callWebserviceForRequest()
            }
        }) { (error) in
            print(error)
            WebServiceManager.hideLoaderView(view: self.view)
            self.showToast(message: "Something Went Wrong")
        }
    }
    func parseRequest(arrRequest:[AnyObject]) {
        print(arrRequest)
        
        if arrUserRequest.count>0{
            arrUserRequest.removeAll()
        }
        for objDict in arrRequest{
        let objRequester = UserModelRequester()
            let objModel = UserRequestModel()
            if let strId = objDict["id"]as? String{
                objModel.id = strId
            }
            
            if let strRequestedFor = objDict["requested_for"] as? Int{
                objModel.requestedFor = strRequestedFor
            }
           if let strRequestedForExtra = objDict["requested_for_extra"] as? String{
                objModel.requestedForExtra = strRequestedForExtra
            }
           if let strRequesterName = objDict["requester_name"] as? String{
            objModel.requesterName = strRequesterName
            }
            if let strReason = objDict["reason"] as? String{
                objModel.reason = strReason
            }
           if  let strInstructions = objDict["instructions"] as? String{
            objModel.instructions = strInstructions
            }
           if  let strStatus = objDict["status"] as? Int{
            objModel.status = strStatus
            }
           if let strPaymentStatus = objDict["payment_status"] as? Int{
            objModel.paymentStatus = strPaymentStatus
            }
           if let strDeliverableType = objDict["deliverable_type"] as? Int{
            objModel.deliverableType = strDeliverableType
            }
           if let strCreatedAt = objDict["created_at"] as? Int{
            objModel.createdAt = strCreatedAt
            }
           if let strUpdatedAt = objDict["updated_at"] as? Int{
            objModel.updatedAt = strUpdatedAt
            }
            if let strRequsted = objDict["requested"] as? String{
                objModel.requested = strRequsted
            }
            
           if let dictRequester = objDict["requester"] as? [String:AnyObject]{
            objRequester.id = dictRequester["id"] as? String
            objRequester.about = dictRequester["about"] as? String
            objRequester.gummyBears = dictRequester["gummy_bears"] as? Int
            objRequester.interests = dictRequester["interests"] as? String
            objRequester.name = dictRequester["name"] as? String
            objRequester.previewUrl = dictRequester["preview_url"] as? [AnyObject]
            objRequester.price = dictRequester["price"] as? Int
            objRequester.responseTime = dictRequester["response_time"] as? Int
            objRequester.userType = dictRequester["user_type"] as? Int
            objRequester.username = dictRequester["username"] as? String
            objModel.requester = objRequester
            let strImgURL = "http://unlo.videous.io/api/user/profile/" + objRequester.username + "/pic"
            
            objModel.imgUrl = strImgURL

            }

            arrUserRequest.append(objModel)
        }
        if userDefault.bool(forKey: AppShareData.UserDefaultKeys.isFirstTime){
            userDefault.set(false, forKey: AppShareData.UserDefaultKeys.isFirstTime)
            userDefault.synchronize()
            self.setUpUI(isFrstTime: true, model: arrUserRequest)

        }else{
            self.setUpUI(isFrstTime: false, model: arrUserRequest)

        }
    }
    func callWebServiceForReject(requestedId:String) {
             //   WebServiceManager.showLoaderView(view: self.view)
        let strAuthToken = UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.access_token)!
           let strTypeToken = UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.token_type)!
         var headers = [String:Any]()
        headers = ["Authorization" : strTypeToken + " " + strAuthToken]

        let url : String = AppShareData.serverURL + AppShareData.EndPoints.requestRjctAcpt + requestedId + "/reject"
        objWebServiceManager.requestPost(strURL: url, params: nil, headers: headers, success: { (response) in
            print(response)
            WebServiceManager.hideLoaderView(view: self.view)

        }) { (error) in
            print(error)
        }
       
    }
    func callWebServiceForAccept(requestedId:String) {
               // WebServiceManager.showLoaderView(view: self.view)
        let url : String = AppShareData.serverURL + AppShareData.EndPoints.requestRjctAcpt + requestedId + "/accept"
        let strAuthToken = UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.access_token)!
              let strTypeToken = UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.token_type)!
            var headers = [String:Any]()
           headers = ["Authorization" : strTypeToken + " " + strAuthToken]

        objWebServiceManager.requestPost(strURL: url, params: nil, headers: headers, success: { (response) in
            print(response)
            let strMsg = response["message"] as? String
            if strMsg == "Success"{
                print("Accept Success")
                
            }
            WebServiceManager.hideLoaderView(view: self.view)

        }) { (error) in
            print(error)
        }
    }
}

/*-----MARK - Set Swipe UI*/
extension homeViewController {
    
    func setUpUI(isFrstTime:Bool , model : [UserRequestModel])
    {
        self.viewRightGuide.isHidden = true
        self.viewNoData.isHidden = true
        self.viewRewindGuide.isHidden = true
        
        // Dynamically create view for each tinder card
        let contentView: (Int, CGRect, UserRequestModel) -> (UIView) = { (index: Int ,frame: CGRect , userModel: UserRequestModel) -> (UIView) in
                let customView = ImageCard(frame: frame)
                customView.userModel = userModel
                return customView
        }
        
        swipeView = TinderSwipeView<UserRequestModel>(frame: viewContainer.bounds, contentView: contentView)
        viewContainer.addSubview(swipeView)
        swipeView.showTinderCards(with: model ,isDummyShow: isFrstTime)
                    
    }
    
    @IBAction func clickONBtnUndo(_ sender: UIButton){
        for objMode in arrUserRequest {
           // model.append(userModel(Username: "Akshay Pruthi", Usermsg: "Akshay Pruthi wants to wish his Friend a \n Happy Birthday", num: "\(1)"))
            print(objMode)
            setUpUI(isFrstTime: false, model: [objMode])

        }
    }
    
}
extension homeViewController : TinderSwipeViewDelegate{
    
    func dummyAnimationDone(strCheck: String) {
        if(strCheck == "left"){
            UIView.transition(with: view, duration: 0.5, options: .curveEaseInOut, animations: {
                self.viewLeftGuide.isHidden = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    self.viewRightGuide.isHidden = false
                })
            })
            
        }else if(strCheck == "right"){
            UIView.transition(with: view, duration: 0.5, options: .curveEaseInOut, animations: {
                    self.viewRightGuide.isHidden = true
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        self.viewRewindGuide.isHidden = false
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                            
                        UIView.transition(with: self.view, duration: 0.5, options: .curveEaseInOut, animations: {
                            self.viewRewindGuide.isHidden = true
                        })
                    })
                })
            })
        }else if(strCheck == "Hide"){
            self.viewRightGuide.isHidden = true
            self.viewLeftGuide.isHidden = true

        }
    }
    
    func didSelectCard(model: Any) {
        print("Selected card")
    }
    
    func fallbackCard(model: Any) {
//        let userModel = model as! UserRequestModel
//        print("Cancelling \(String(describing: userModel.Username))")
        print("Cancelling")

    }
    
    func cardGoesLeft(model: Any) {
        let userModel = model as! UserRequestModel
        
//        print("Watchout Left \(String(describing: userModel.Username))")
     let strID = userModel.id!
        callWebServiceForReject(requestedId: strID)

    }
    
    func cardGoesRight(model : Any) {
        let userModel = model as! UserRequestModel
    print("Watchout Right")
        let strID = userModel.id!

        callWebServiceForAccept(requestedId: strID)

    }
    
    func undoCardsDone(model: Any) {
//        let userModel = model as! userModel
      //  print("Reverting done \(String(describing: userModel.Username))")
        print("Reverting done")
    }
    
    func endOfCardsReached() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
                self.viewNoData.isHidden = false
                print("End of all cards")
        })
    }
    
    func currentCardStatus(card object: Any, distance: CGFloat) {
        if distance == 0 {
        }else{
            let value = Float(min(abs(distance/100), 1.0) * 5)
            let _ = distance > 0  ? 2.5 + (value * 5) / 10  : 2.5 - (value * 5) / 10
        }
    }
}

extension UIView {
    
    func superview<T>(of type: T.Type) -> T? {
        return superview as? T ?? superview.map { $0.superview(of: type)! }
    }
    
    func subview<T>(of type: T.Type) -> T? {
        return subviews.compactMap { $0 as? T ?? $0.subview(of: type) }.first
    }
}

//MArk -- bottom actions
extension homeViewController : UIGestureRecognizerDelegate{
    
    func swipeToPop() {

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false;
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self;
    }

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }

    
    @IBAction func clcikONBottomBtn(_ sender : UIButton){
        
        if(sender.tag == 20){
            
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "requestViewController") as! requestViewController
            self.navigationController?.pushViewController(dest, animated: false)
            
        }
        else if(sender.tag == 30){
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "profileViewController") as! profileViewController
            self.navigationController?.pushViewController(dest, animated: false)
            
        }
        
        
    }

    
}
