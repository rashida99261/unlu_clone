//
//  myVideosViewController.swift
//  Unlu
//
//  Created by Apple on 20/04/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class myVideosViewController: UIViewController {
    
    @IBOutlet weak var collPhotos : UICollectionView!
    var estimateWidth = 173.0
    var cellMarginSize = 8

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupGridView()

    }
    
    @IBAction func clionBackBtn(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupGridView() {
           let flow = collPhotos?.collectionViewLayout as! UICollectionViewFlowLayout
           flow.minimumInteritemSpacing = CGFloat(self.cellMarginSize)
           flow.minimumLineSpacing = CGFloat(self.cellMarginSize)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.setupGridView()
        DispatchQueue.main.async {
            self.collPhotos.reloadData()
        }
    }

}
extension myVideosViewController : UICollectionViewDelegate , UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collPhotos.dequeueReusableCell(withReuseIdentifier: "collPhotos", for: indexPath) as! collPhotos
        return cell
    }
}

extension myVideosViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.calculateWith()
        return CGSize(width: width, height: 220)
    }
    
    func calculateWith() -> CGFloat {
        let estimatedWidth = CGFloat(estimateWidth)
        let cellCount = floor(CGFloat(self.view.frame.size.width / estimatedWidth))
        
        let margin = CGFloat(cellMarginSize * 2)
        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
        
        return width
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
}


class collPhotos : UICollectionViewCell {
    
}
