//
//  profileViewController.swift
//  Unlu
//
//  Created by Apple on 11/04/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class profileViewController: UIViewController {
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!

    @IBOutlet weak var imgProfile: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if  AppShareData.Variable.isFromLogin{
                  callWebserviceForProfile(header: [:])
           // callWebserviceForProfileImg(header: [:])
              }else{
              let strAuthToken=UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.access_token)!
                  let strTypeToken = UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.token_type)!
                  
                      let headers = ["Authorization" : strTypeToken + " " + strAuthToken]

                  callWebserviceForProfile(header: headers)
//callWebserviceForProfileImg(header: headers)
              }
        //http://unlo.videous.io/api/user/profile/dianapenty/pic

    }
    
    func callWebserviceForProfile(header:[String:Any]){
        WebServiceManager.showLoaderView(view: self.view)
        let strUserName = userDefault.string(forKey: AppShareData.UserDefaultKeys.userName)!
        let url : String = AppShareData.serverURL + AppShareData.EndPoints.profileDetail + strUserName//"dianapenty"
        print(url)
        objWebServiceManager.requestGet(strURL: url, params: nil, headers: header, success: { (response) in
            print(response)
            WebServiceManager.hideLoaderView(view: self.view)

            let strStatus = response["statusCode"] as? Int
            if strStatus == 0 {
                let dict = response["payload"] as? [String:Any]
                self.lblFullName.text = dict!["name"]as?String
                self.lblUserName.text = dict!["username"]as?String
                let strUserName = dict!["username"]as?String
                self.imgProfile.sd_setImage(with: URL(string: "http://unlo.videous.io/api/user/profile/\(strUserName!)/pic"), placeholderImage: #imageLiteral(resourceName: "profile_unselect"))

            }
        }) { (error) in
            print(error)
            WebServiceManager.hideLoaderView(view: self.view)
            self.showToast(message: "Something Went Wrong")

        }
    }
    
}


//MArk -- bottom actions
extension profileViewController {
    
    @IBAction func clcikONBottomBtn(_ sender : UIButton){
        
        if(sender.tag == 10){
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(dest, animated: false)
        }
        else if(sender.tag == 20){
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "requestViewController") as! requestViewController
            self.navigationController?.pushViewController(dest, animated: false)
        }
    }
    
    @IBAction func clickOnBtnList(_ sender: UIButton){
        
        if(sender.tag == 10){
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "earningViewController") as! earningViewController
            self.navigationController?.pushViewController(dest, animated: true)
        }
        else if(sender.tag == 20){
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "myVideosViewController") as! myVideosViewController
            self.navigationController?.pushViewController(dest, animated: true)
        }
        else if(sender.tag == 30){
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "payAccountVC") as! payAccountVC
            self.navigationController?.pushViewController(dest, animated: true)
            
        }
        else if(sender.tag == 40){
            //support page
        }
        else if(sender.tag == 50){
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! loginViewController
            self.navigationController?.pushViewController(dest, animated: true)
            
        }
    }
}
