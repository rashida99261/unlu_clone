
import UIKit

class completeVideoViewController: UIViewController {
    
    @IBOutlet weak var viewgHght : NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickONBackBtn(_ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickONDropDown(_ sender : UIButton)
    {
        if(sender.isSelected == true)
        {
            sender.isSelected = false
            UIView.transition(with: view, duration: 0.5, options: .curveEaseInOut, animations: {
                        self.viewgHght.constant = 40
        
            })
            
        }
        else if(sender.isSelected == false)
        {
            sender.isSelected = true
            UIView.transition(with: view, duration: 0.5, options: .curveEaseInOut, animations: {
                        self.viewgHght.constant = 480
            
            })
        }
    }
}
