//
//  userRequestViewController.swift
//  Unlu
//
//  Created by Apple on 22/04/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class userRequestViewController: UIViewController {
    
    @IBOutlet weak var scrollVw : UIScrollView!
    
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imgHeader : UIImageView!
    @IBOutlet weak var lblTitle : UILabel!

    
    let headerViewMaxHeight: CGFloat = 350
    let headerViewMinHeight: CGFloat = 44 + UIApplication.shared.statusBarFrame.height


    @IBOutlet weak var titleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoImageView: UIImageView!


    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func clickONBtnAction(_ sender: UIButton){
        
        if(sender.tag == 10){
            self.navigationController?.popViewController(animated: true)
        }
        else if(sender.tag == 20){
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "uploadViewController") as! uploadViewController
            dest.isPopFrom = "user"
           // dest.modalPresentationStyle = .overCurrentContext
          //  self.present(dest, animated: true, completion: nil)
            self.navigationController?.pushViewController(dest, animated: true)
        }
        
    }

}

extension userRequestViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        // Always update the previous values
        let y: CGFloat = scrollView.contentOffset.y
        let newHeaderViewHeight: CGFloat = headerHeightConstraint.constant - y
        if newHeaderViewHeight > headerViewMaxHeight {
               
                UIView.animate(withDuration: 0.2, animations: {
                    self.headerHeightConstraint.constant = self.headerViewMaxHeight
                    self.updateViewsinHeader()
                    self.view.layoutIfNeeded()
                })

        } else if newHeaderViewHeight < headerViewMinHeight {
            
            UIView.animate(withDuration: 0.2, animations: {
                       self.headerHeightConstraint.constant = self.headerViewMinHeight
                       self.updateViewsinHeader()
                       self.view.layoutIfNeeded()
            })

        } else {
                
               UIView.animate(withDuration: 0.2, animations: {
                self.headerHeightConstraint.constant = newHeaderViewHeight
                   scrollView.contentOffset.y = 0 // block scroll view/
                   self.updateViewsinHeader()
                   self.view.layoutIfNeeded()
               })
        }
    }
    
    func updateViewsinHeader() {
           let range = self.headerViewMaxHeight - self.headerViewMinHeight
           let openAmount = self.headerHeightConstraint.constant - self.headerViewMinHeight
           let percentage = openAmount / range
        
                self.titleTopConstraint.constant = -openAmount + 20
               self.logoImageView.alpha = percentage

       }
}
