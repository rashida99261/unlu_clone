//
//  requestViewController.swift
//  Unlu
//
//  Created by Apple on 11/04/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
var isUpload = ""

class requestViewController: UIViewController {
    
    @IBOutlet weak var viewNoReq : UIView!
    @IBOutlet weak var viewTbl : UIView!
    @IBOutlet weak var tblReqList : UITableView!
    
    @IBOutlet weak var viewSuccess : UIView!
    
    
    var arrReqData : [[PandingRequestRootClass]] = []
    var section = [String]()
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        count = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if  AppShareData.Variable.isFromLogin{
            callWebServiceForPandingRequest(header: [:])
        }else{
            let strAuthToken=UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.access_token)!
            let strTypeToken = UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.token_type)!
            
            let headers = ["Authorization" : strTypeToken + " " + strAuthToken]
            
            callWebServiceForPandingRequest(header: headers)
        }
        self.setUpUI()
    }
    
    @IBAction func clickONCompleteProfileBtn(_ sender : UIButton){
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "userRequestViewController") as! userRequestViewController
        self.navigationController?.pushViewController(dest, animated: true)
    }
    
    @IBAction func clickOnRecordMore(_ sender : UIButton){
        //let dest = self.storyboard?.instantiateViewController(identifier: "uploadViewController") as! uploadViewController
        //dest.isPopFrom = "request"
        //self.navigationController?.pushViewController(dest, animated: true)
        
        UIView.transition(with: view, duration: 0.5, options: .curveEaseInOut, animations: {
                self.viewSuccess.isHidden = true
                       
        })
    }
    
    
    func setUpUI()
    {
        tblReqList.tableFooterView = UIView()

        self.viewSuccess.isHidden = true
               
        if(isUpload == "true"){
                section = ["UPLOADING", "PENDING", "COMPLETED"]
                //arrReqData = [["<User-Name>"], ["<User-Name>", "<User-Name>"], ["<User-Name>"]]
        }
        else{
         //   section = ["PENDING", "COMPLETED"]
          //  arrReqData = [["<User-Name>", "<User-Name>"], ["<User-Name>"]]
            section = ["PENDING"]

//        arrReqData = [["<User-Name>", "<User-Name>"]]
        }
               
        if(arrReqData.count == 0){
                self.viewNoReq.isHidden = false
                self.viewTbl.isHidden = true
        }else{
                self.viewNoReq.isHidden = true
                self.viewTbl.isHidden = false
        }
        

    }
    
    @IBAction func handleTap(_ sender : UIButton){
        
        UIView.transition(with: view, duration: 0.5, options: .curveEaseInOut, animations: {
                self.viewSuccess.isHidden = true
                       
        })
    }
}

//MArk -- bottom actions
extension requestViewController {
    
    @IBAction func clcikONBottomBtn(_ sender : UIButton){
        
        if(sender.tag == 10){
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(dest, animated: false)
        }
        else if(sender.tag == 30){
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "profileViewController") as! profileViewController
            self.navigationController?.pushViewController(dest, animated: false)
            
        }
    }

    
}
//MARK:- callWebservice
extension requestViewController{
    func callWebServiceForPandingRequest(header:[String:Any]) {
    WebServiceManager.showLoaderView(view: self.view)
        let url : String = AppShareData.serverURL + AppShareData.EndPoints.requestPanding + String(count)//"dianapenty"
        print(url)
        objWebServiceManager.requestGet(strURL: url, params: nil, headers: header, success: { (response) in
            print(response)
            WebServiceManager.hideLoaderView(view: self.view)
            
            let strStatus = response["statusCode"] as? Int
            if strStatus == 0 {
             let dict = response["payload"] as? [AnyObject]
                if dict!.isEmpty{
                    print("blank")
                }else{
                    print(dict!)
                    self.parseRequest(arrRequest: dict!)
                }

            }else{
                
            }
            /*
             ["statusCode": 0, "message": Success, "payload": <__NSArrayI 0x600000b5aac0>(
             {
                 "created_at" = 1589463819862;
                 "deliverable_type" = 400;
                 id = 5ebd4b0b5c03128c5ce6c5ea;
                 instructions = "dhgjg jdjdjfjcjdjdjdrn fjjfjfj";
                 "payment_status" = 210;
                 reason = 120;
                 requested = "<null>";
                 "requested_for" = 300;
                 "requested_for_extra" = Pooja;
                 requester =     {
                     about = "";
                     "gummy_bears" = 0;
                     id = 5ebd359c5c03123dc2e6c5cb;
                     interests = "";
                     name = "Apoorva Gangrade";
                     "preview_url" =         (
                     );
                     price = 1000;
                     "response_time" = 1589458332453;
                     "user_type" = 0;
                     username = apoorva;
                 };
                 "requester_name" = Hena;
                 status = 20;
                 "updated_at" = 1589532661015;
             },
             {
                 "created_at" = 1589375766979;
                 "deliverable_type" = 400;
                 id = 5ebbf3160c53cc560291350c;
                 instructions = djidnf;
                 "payment_status" = 210;
                 reason = 110;
                 requested = "<null>";
                 "requested_for" = 310;
                 "requested_for_extra" = "<null>";
                 requester =     {
                     about = "";
                     "gummy_bears" = 0;
                     id = 5e7c8de5e28c510011927fd6;
                     interests = "";
                     name = "Shubham Rohilla";
                     "preview_url" =         (
                     );
                     price = 1000;
                     "response_time" = 1589630647076;
                     "user_type" = 0;
                     username = shubham;
                 };
                 "requester_name" = djodkdpf;
                 status = 20;
                 "updated_at" = 1589377296861;
             },
             {
                 "created_at" = 1589375747454;
                 "deliverable_type" = 400;
                 id = 5ebbf3030c53cc859c91350a;
                 instructions = bdjdnfnf;
                 "payment_status" = 210;
                 reason = 110;
                 requested = "<null>";
                 "requested_for" = 310;
                 "requested_for_extra" = "<null>";
                 requester =     {
                     about = "";
                     "gummy_bears" = 0;
                     id = 5e7c8de5e28c510011927fd6;
                     interests = "";
                     name = "Shubham Rohilla";
                     "preview_url" =         (
                     );
                     price = 1000;
                     "response_time" = 1589630647077;
                     "user_type" = 0;
                     username = shubham;
                 };
                 "requester_name" = ndjdnf;
                 status = 20;
                 "updated_at" = 1589532663325;
             }
             )
             ]*/
        }) { (error) in
            print(error)
            WebServiceManager.hideLoaderView(view: self.view)
        }
    }
    func parseRequest(arrRequest:[AnyObject]) {
        print(arrRequest)
        if arrReqData.count > 0 {
            arrReqData.removeAll()
        }
        for objRequest in arrRequest{
            let objModel = PandingRequestRootClass.init(fromDictionary: objRequest as! [String : Any])
            print(objModel)
            arrReqData.append([objModel])
        }
        tblReqList.reloadData()
        setUpUI()
    }
}
extension requestViewController : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return section.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrReqData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblReqList.dequeueReusableCell(withIdentifier: "reqTblCell") as! reqTblCell
              let objModel = arrReqData[indexPath.row]
                for obj in objModel{
                    let objUserName = obj.requester
                    
                    cell.lblName.text = objUserName?.name!
                    let strUserName = objUserName?.username
                    let strDate = obj.createdAt!
                    cell.imgUser.sd_setImage(with: URL(string: "http://unlo.videous.io/api/user/profile/\(strUserName!)/pic"), placeholderImage: #imageLiteral(resourceName: "profile_unselect"))
                    let date = NSDate(timeIntervalSince1970: Double(strDate))
                      let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
                    dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                    dateFormatter.timeZone = .current
                    let localDate = dateFormatter.string(from: date as Date)

print(localDate)
                }
        //cell.lblName.text = self.arrReqData[indexPath.section][indexPath.row]
        
        if(isUpload == "true"){
            
            if(indexPath.section == 0)
            {
                       cell.viewUpload.isHidden = false
                       cell.viewRecord.isHidden = true
                       cell.viewPlay.isHidden = true
                       
                       DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                               self.viewSuccess.isHidden = false
                       })
                       
             }
            else if(indexPath.section == 1)
            {
                      cell.viewUpload.isHidden = true
                       cell.viewRecord.isHidden = false
                       cell.viewPlay.isHidden = true
            }
            else if(indexPath.section == 2)
            {
                       cell.viewUpload.isHidden = true
                       cell.viewRecord.isHidden = true
                       cell.viewPlay.isHidden = false
            }
        }
        else{
             if(indexPath.section == 0)
            {
                    cell.viewUpload.isHidden = true
                     cell.viewRecord.isHidden = false
                     cell.viewPlay.isHidden = true
            }
            else if(indexPath.section == 1)
            {
                    cell.viewUpload.isHidden = true
                    cell.viewRecord.isHidden = true
                    cell.viewPlay.isHidden = false
            }
        }
        return cell
    }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tblReqList.dequeueReusableCell(withIdentifier: "reqTblHeaderCell") as! reqTblHeaderCell
        headerCell.lblTitle.text = self.section[section]
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(isUpload == "true"){
            
            if(indexPath.section == 0)
            {
                
            }
            else if(indexPath.section == 1)
            {
                let dest = self.storyboard?.instantiateViewController(withIdentifier: "completeVideoViewController") as! completeVideoViewController
                self.navigationController?.pushViewController(dest, animated: true)
            }
            else if(indexPath.section == 2)
            {
                 let dest = self.storyboard?.instantiateViewController(withIdentifier: "completeVideoViewController") as! completeVideoViewController
                self.navigationController?.pushViewController(dest, animated: true)
            }
            
        }
        else{
            if(indexPath.section == 0)
            {
                let dest = self.storyboard?.instantiateViewController(withIdentifier: "userRequestViewController") as! userRequestViewController
                self.navigationController?.pushViewController(dest, animated: true)
            }
            else if(indexPath.section == 1)
            {
                let dest = self.storyboard?.instantiateViewController(withIdentifier: "completeVideoViewController") as! completeVideoViewController
                self.navigationController?.pushViewController(dest, animated: true)
            }
        }
    }

}

class reqTblCell : UITableViewCell
{
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var viewUpload : UIView!
    @IBOutlet weak var viewRecord : UIView!
    @IBOutlet weak var viewPlay : UIView!
    
}

class reqTblHeaderCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
}
