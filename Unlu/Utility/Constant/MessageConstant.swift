//
//  MessageConstant.swift
//  Unlu
//
//  Created by Apple on 08/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit

let NoNetwork: String = "No network connection"
let alertTitleOops: String = "Oops!"
let somethingWentWrong = "Something went wrong, Please check your internet connection."
let alertTitle: String = "Alert"

let objWebServiceManager:WebServiceManager = WebServiceManager()
let userDefault = UserDefaults.standard

//MARK: - Alerts
func showAlertVC(title:String,message:String,controller:UIViewController) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let subView = alertController.view.subviews.first!
    let alertContentView = subView.subviews.first!
    alertContentView.backgroundColor = UIColor.gray
    alertContentView.layer.cornerRadius = 20
    let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(OKAction)
    controller.present(alertController, animated: true, completion: nil)
}

//MARK:- Toast Msg
extension UIViewController{
    
func showToast(message : String) {

       let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-120, width: 250, height: 35))
       toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
       toastLabel.textColor = UIColor.white
       toastLabel.numberOfLines = 0
       toastLabel.textAlignment = .center;
       toastLabel.font = UIFont(name: "Gilroy-Bold", size: 15.0)
       toastLabel.text = message
       toastLabel.alpha = 1.0
       toastLabel.layer.cornerRadius = 10;
       toastLabel.clipsToBounds  =  true
       self.view.addSubview(toastLabel)
       UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
           toastLabel.alpha = 0.0
       }, completion: {(isCompleted) in
           toastLabel.removeFromSuperview()
       })
   }
}
