//
//  WebServiceManager.swift
//  Unlu
//
//  Created by Apple on 08/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

var  strAuthToken : String = ""
var strTypeToken : String = ""

class WebServiceManager: NSObject {
    //MARK: - Shared object
     
     private static var sharedNetworkManager: WebServiceManager = {
         let networkManager = WebServiceManager()
         return networkManager
     }()
     // MARK: - Accessors
     class func sharedObject() -> WebServiceManager {
         return sharedNetworkManager
     }
     
     func showAlert(message: String = "", title: String , controller: UIWindow) {
         DispatchQueue.main.async(execute: {
             let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
             let subView = alertController.view.subviews.first!
             let alertContentView = subView.subviews.first!
             alertContentView.backgroundColor = UIColor.gray
             alertContentView.layer.cornerRadius = 20
             let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
             alertController.addAction(OKAction)
             UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
         })
     }
    static func showLoaderView(view: UIView){
        let spinnerActivity = MBProgressHUD.showAdded(to: view, animated: true);
        spinnerActivity.mode = .indeterminate
        spinnerActivity.backgroundView.backgroundColor = .clear
        spinnerActivity.backgroundColor = .clear
        spinnerActivity.bezelView.backgroundColor = .clear
           spinnerActivity.backgroundView.style = .solidColor
        spinnerActivity.backgroundView.color = .clear
        spinnerActivity.isUserInteractionEnabled = false
    }
    
    static func hideLoaderView(view: UIView){
        MBProgressHUD.hide(for: view, animated: true)
    }
    
     public func requestPost(strURL:String, params : [String:Any]?, headers : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
         
         if !NetworkReachabilityManager()!.isReachable{
            // self.StopIndicator()
             let app = UIApplication.shared.delegate as? AppDelegate
             let window = app?.window
             showAlert(message: "", title: NoNetwork ,controller: window!)
             return
         }
       
//       let headers = ["Content-Type": "application/x-www-form-urlencoded"]
//          if UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.access_token)==nil {
//                        strAuthToken=""
//                    }else{
//                        strAuthToken=UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.access_token)!
//                     strTypeToken = UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.token_type)!
//        }
//                var headers = [String:Any]()
//                if userDefault.bool(forKey: AppShareData.UserDefaultKeys.isFirstTime){
//                    headers = [:]
//                }else{
//                    headers = ["Authorization" : strTypeToken + " " + strAuthToken]
//                }
        
        Alamofire.request(strURL, method: .post, parameters: params, headers: headers as? HTTPHeaders).responseJSON { responseObject in
           
             if responseObject.result.isSuccess {

                 do {
                                 let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                                 success(dictionary as! Dictionary<String, Any>)
                                 print(dictionary)
                             }catch{
                             
                 }
                         }
                         if responseObject.result.isFailure {
                        //     self.StopIndicator()
                             let error : Error = responseObject.result.error!
                             failure(error)
                         }
                 }
     }
    
    public func requestGet(strURL:String, params : [String : AnyObject]?, headers : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
            if !NetworkReachabilityManager()!.isReachable{
                //self.StopIndicator()
                let app = UIApplication.shared.delegate as? AppDelegate
                let window = app?.window
                showAlert(message: "", title: NoNetwork , controller: window!)
                return
            }
        
        if UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.access_token)==nil {
                strAuthToken=""
            }else{
                strAuthToken=UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.access_token)!
             strTypeToken = UserDefaults.standard.string(forKey:AppShareData.UserDefaultKeys.token_type)!
<<<<<<< HEAD
        }
        var headers = [String:Any]()
       // if userDefault.bool(forKey: AppShareData.UserDefaultKeys.isFirstTime){
           // headers = [:]
      //  }else{
            headers = ["Authorization" : strTypeToken + " " + strAuthToken]
      //  }
=======
}
//        var headers = [String:Any]()
//        if userDefault.bool(forKey: AppShareData.UserDefaultKeys.isFirstTime){
//            headers = [:]
//        }else{
//            headers = ["Authorization" : strTypeToken + " " + strAuthToken]
//        }
>>>>>>> 621b3d6b094a7e8aaf1344186ff6591b72661647

        Alamofire.request(strURL, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers as? HTTPHeaders).responseJSON { responseObject in
               
               // self.StopIndicator()

                if responseObject.result.isSuccess {
    //                let resJson = JSON(responseObject.result.value!)
    //                success(resJson)
                    do {
                        let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        success(dictionary as! Dictionary<String, Any>)
                        print(dictionary)
                    }catch{
                        
                    }
                }
                if responseObject.result.isFailure {
                  //  self.StopIndicator()
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
            }
        }

}
