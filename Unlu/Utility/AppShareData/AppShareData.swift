//
//  AppShareData.swift
//  Unlu
//
//  Created by Apple on 08/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SystemConfiguration

class AppShareData: NSObject, UIAlertViewDelegate {

    var objAppDelegate:AppDelegate = AppDelegate()

    //MARK: - Shared object
    
    private static var sharedManager: AppShareData = {
        let manager = AppShareData()
        return manager
    }()
    
    // MARK: - Accessors
    class func sharedObject() -> AppShareData {
        return sharedManager
    }
    
    static let serverURL = "https://unlo.videous.io"
    static let deviceID : String = UIDevice.current.identifierForVendor!.uuidString

    //MARK: - API End Point
   struct EndPoints{
     static let loginApi = "/api/token"
    static let profileDetail = "/api/user/profile/"
    static let FreshVideoReques = "/api/requests/fresh/1"
    static let requestRjctAcpt = "/api/request/"
    static let requestPanding = "/api/requests/accepted/"
    }
    //MARK: -  User Defautls
    struct UserDefaultKeys{
        static let userToken  =  "UserToken"
        static let access_token = "access_token"
        static let expires_in = "expires_in"
        static let refresh_token = "refresh_token"
        static let scope = "scope"
        static let token_type = "token_type"
        static var userName = "userName"
        static let fullName = "fullName"
        static let isFirstTime = "isFirstTime"

        
    }
    // MARK:- Variable
    struct Variable {
        static let kAuthToken = "authToken"
        static var isFromLogin = false
    }
    //MARK:- Storyboard
    struct StoryBoard{
        static let main = UIStoryboard(name: "Main", bundle: nil)

    }
//MARK:- Screen Dimension
    struct ScreenDimension {
        static let width                    = UIScreen.main.bounds.size.width
        static let height                   = UIScreen.main.bounds.size.height
    }
}
