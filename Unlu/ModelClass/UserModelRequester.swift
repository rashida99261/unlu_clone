//
//  UserModelRequester.swift
//  Unlu
//
//  Created by Apple on 14/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UserModelRequester: NSObject {
   
    var id : String!
    var name : String!
    var username : String!
    var about : String!
    var price : Int!
    var responseTime : Int!
    var gummyBears : Int!
    var userType : Int!
    var interests : String!
    var previewUrl : [AnyObject]!
}
