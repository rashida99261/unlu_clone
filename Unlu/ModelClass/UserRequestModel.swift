//
//  UserRequestModel.swift
//  Unlu
//
//  Created by Apple on 14/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UserRequestModel: NSObject {
    var num : String!
    var id : String!
    var requester : UserModelRequester!
    var requested : String!
    var requestedFor : Int!
    var requestedForExtra : String!
    var requesterName : String!
    var reason : String!
    var instructions : String!
    var status : Int!
    var paymentStatus : Int!
    var deliverableType : Int!
    var createdAt : Int!
    var updatedAt : Int!
    var imgUrl : String!

}
