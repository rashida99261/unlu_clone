//
//  PandingRequestRequester.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 16, 2020

import Foundation


class PandingRequestRequester : NSObject, NSCoding{

    var about : String!
    var gummyBears : Int!
    var id : String!
    var interests : String!
    var name : String!
    var previewUrl : [AnyObject]!
    var price : Int!
    var responseTime : Int!
    var userType : Int!
    var username : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        about = dictionary["about"] as? String
        gummyBears = dictionary["gummy_bears"] as? Int
        id = dictionary["id"] as? String
        interests = dictionary["interests"] as? String
        name = dictionary["name"] as? String
        price = dictionary["price"] as? Int
        responseTime = dictionary["response_time"] as? Int
        userType = dictionary["user_type"] as? Int
        username = dictionary["username"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if about != nil{
            dictionary["about"] = about
        }
        if gummyBears != nil{
            dictionary["gummy_bears"] = gummyBears
        }
        if id != nil{
            dictionary["id"] = id
        }
        if interests != nil{
            dictionary["interests"] = interests
        }
        if name != nil{
            dictionary["name"] = name
        }
        if price != nil{
            dictionary["price"] = price
        }
        if responseTime != nil{
            dictionary["response_time"] = responseTime
        }
        if userType != nil{
            dictionary["user_type"] = userType
        }
        if username != nil{
            dictionary["username"] = username
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        about = aDecoder.decodeObject(forKey: "about") as? String
        gummyBears = aDecoder.decodeObject(forKey: "gummy_bears") as? Int
        id = aDecoder.decodeObject(forKey: "id") as? String
        interests = aDecoder.decodeObject(forKey: "interests") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        previewUrl = aDecoder.decodeObject(forKey: "preview_url") as? [AnyObject]
        price = aDecoder.decodeObject(forKey: "price") as? Int
        responseTime = aDecoder.decodeObject(forKey: "response_time") as? Int
        userType = aDecoder.decodeObject(forKey: "user_type") as? Int
        username = aDecoder.decodeObject(forKey: "username") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if about != nil{
            aCoder.encode(about, forKey: "about")
        }
        if gummyBears != nil{
            aCoder.encode(gummyBears, forKey: "gummy_bears")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if interests != nil{
            aCoder.encode(interests, forKey: "interests")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if previewUrl != nil{
            aCoder.encode(previewUrl, forKey: "preview_url")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if responseTime != nil{
            aCoder.encode(responseTime, forKey: "response_time")
        }
        if userType != nil{
            aCoder.encode(userType, forKey: "user_type")
        }
        if username != nil{
            aCoder.encode(username, forKey: "username")
        }
    }
}