//
//  PandingRequestRootClass.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 16, 2020

import Foundation


class PandingRequestRootClass : NSObject, NSCoding{

    var createdAt : Int!
    var deliverableType : Int!
    var id : String!
    var instructions : String!
    var paymentStatus : Int!
    var reason : String!
    var requested : AnyObject!
    var requestedFor : Int!
    var requestedForExtra : AnyObject!
    var requester : PandingRequestRequester!
    var requesterName : String!
    var status : Int!
    var updatedAt : Int!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        createdAt = dictionary["created_at"] as? Int
        deliverableType = dictionary["deliverable_type"] as? Int
        id = dictionary["id"] as? String
        instructions = dictionary["instructions"] as? String
        paymentStatus = dictionary["payment_status"] as? Int
        reason = dictionary["reason"] as? String
        requested = dictionary["requested"] as? AnyObject
        requestedFor = dictionary["requested_for"] as? Int
        requestedForExtra = dictionary["requested_for_extra"] as? AnyObject
        requesterName = dictionary["requester_name"] as? String
        status = dictionary["status"] as? Int
        updatedAt = dictionary["updated_at"] as? Int
        if let requesterData = dictionary["requester"] as? [String:Any]{
            requester = PandingRequestRequester(fromDictionary: requesterData)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if deliverableType != nil{
            dictionary["deliverable_type"] = deliverableType
        }
        if id != nil{
            dictionary["id"] = id
        }
        if instructions != nil{
            dictionary["instructions"] = instructions
        }
        if paymentStatus != nil{
            dictionary["payment_status"] = paymentStatus
        }
        if reason != nil{
            dictionary["reason"] = reason
        }
        if requested != nil{
            dictionary["requested"] = requested
        }
        if requestedFor != nil{
            dictionary["requested_for"] = requestedFor
        }
        if requestedForExtra != nil{
            dictionary["requested_for_extra"] = requestedForExtra
        }
        if requesterName != nil{
            dictionary["requester_name"] = requesterName
        }
        if status != nil{
            dictionary["status"] = status
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        if requester != nil{
            dictionary["requester"] = requester.toDictionary()
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? Int
        deliverableType = aDecoder.decodeObject(forKey: "deliverable_type") as? Int
        id = aDecoder.decodeObject(forKey: "id") as? String
        instructions = aDecoder.decodeObject(forKey: "instructions") as? String
        paymentStatus = aDecoder.decodeObject(forKey: "payment_status") as? Int
        reason = aDecoder.decodeObject(forKey: "reason") as? String
        requested = aDecoder.decodeObject(forKey: "requested") as? AnyObject
        requestedFor = aDecoder.decodeObject(forKey: "requested_for") as? Int
        requestedForExtra = aDecoder.decodeObject(forKey: "requested_for_extra") as? AnyObject
        requester = aDecoder.decodeObject(forKey: "requester") as? PandingRequestRequester
        requesterName = aDecoder.decodeObject(forKey: "requester_name") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Int
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? Int
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if deliverableType != nil{
            aCoder.encode(deliverableType, forKey: "deliverable_type")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if instructions != nil{
            aCoder.encode(instructions, forKey: "instructions")
        }
        if paymentStatus != nil{
            aCoder.encode(paymentStatus, forKey: "payment_status")
        }
        if reason != nil{
            aCoder.encode(reason, forKey: "reason")
        }
        if requested != nil{
            aCoder.encode(requested, forKey: "requested")
        }
        if requestedFor != nil{
            aCoder.encode(requestedFor, forKey: "requested_for")
        }
        if requestedForExtra != nil{
            aCoder.encode(requestedForExtra, forKey: "requested_for_extra")
        }
        if requester != nil{
            aCoder.encode(requester, forKey: "requester")
        }
        if requesterName != nil{
            aCoder.encode(requesterName, forKey: "requester_name")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
    }
}