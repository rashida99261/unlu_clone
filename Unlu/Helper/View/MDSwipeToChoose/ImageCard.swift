//
//  ImageCard.swift
//  CardSlider
//
//  Created by Saoud Rizwan on 2/27/17.
//  Copyright © 2017 Saoud Rizwan. All rights reserved.
//

import UIKit

class ImageCard: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imgCard : UIImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblMessage : UILabel!
    
    var userModel : UserRequestModel! {
           didSet{
            self.lblName.text = userModel.requesterName
            self.lblMessage.text = userModel.instructions
           //objDict?.about
          //  self.imgCard.image = UIImage(named:String(Int(1 + arc4random() % (8 - 1))))
             imgCard.sd_setImage(with: URL(string: "http://unlo.videous.io/api/user/profile/dianapenty/pic"), placeholderImage: #imageLiteral(resourceName: "profile_unselect"))
           }
       }
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    func commonInit() {
        Bundle.main.loadNibNamed(ImageCard.className, owner: self, options: nil)
        contentView.fixInView(self)
    }
    

    
}

extension UIView
{
    func fixInView(_ container: UIView!) -> Void{
       
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}

extension NSObject {
    
    class var className: String {
        return String(describing: self)
    }
}
